package com._28ftb.plugin;

import com.comphenix.protocol.PacketType;
import com.comphenix.protocol.ProtocolLibrary;
import com.comphenix.protocol.events.ListenerPriority;
import com.comphenix.protocol.events.PacketAdapter;
import com.comphenix.protocol.events.PacketEvent;
import lombok.Getter;
import org.bukkit.plugin.java.JavaPlugin;

public final class BattleRoyale extends JavaPlugin {
    @Getter
    private static BattleRoyale INSTANCE;

    public BattleRoyale() {
        super();
        INSTANCE = this;
    }

    @Override
    public void onEnable() {
        // 起動処理

    }

    @Override
    public void onDisable() {
        // 終了処理
    }
}
