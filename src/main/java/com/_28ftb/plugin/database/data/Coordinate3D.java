package com._28ftb.plugin.database.data;

import com._28ftb.plugin.BattleRoyale;
import lombok.AllArgsConstructor;
import lombok.Getter;
import org.bukkit.Location;
import org.bukkit.World;

/**
 * 3次元空間上の座標 //TODO: 直接Locationにしたほうがいい気がする.
 */
@AllArgsConstructor
public class Coordinate3D {
    @Getter
    private double x;
    @Getter
    private double y;
    @Getter
    private double z;

    public Location toLocation(World world){
        return new Location(world,x,y,z);
    }

    public Location toLocation(String world){
        return toLocation(BattleRoyale.getINSTANCE().getServer().getWorld(world));
    }
}
