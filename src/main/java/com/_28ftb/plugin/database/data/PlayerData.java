package com._28ftb.plugin.database.data;

import com._28ftb.plugin.clubs.data.ClubType;
import lombok.Data;

@Data
public class PlayerData {
    ClubType club; //所属している部活
    PlayerStatus status; //プレイヤーのゲーム参加状況
    PlayerTeam team; //チーム戦用のチーム
    int killCount; //殺した数
    int DeathTime; //TODO: サーバーはTickで動いてるので、同時死亡はTickが同じかで判断するのが良いと思ふ。

}
