package com._28ftb.plugin.database.data;

import com._28ftb.plugin.BattleRoyale;
import lombok.Data;
import lombok.Getter;
import lombok.Setter;
import org.bukkit.ChatColor;
import org.bukkit.World;
import org.bukkit.inventory.ItemStack;

import java.util.ArrayList;
import java.util.List;

@Data
public class Settings {

    public static World world = BattleRoyale.getINSTANCE().getServer().getWorld("world");

    public static String prefix = ChatColor.GRAY + "[BattleRoyale]";

    private static List<Coordinate3D> chestCoordinate; //TODO: 直接Locationにしたほうがいい気がする。
    private static List<Coordinate3D> startPoint;
    private static List<ItemStack> chestItem;
    private static List<ItemStack> firstItem;

    public static Coordinate3D stagePos1;
    public static Coordinate3D stagePos2;

    public static Coordinate3D spawnPoint; //サーバーにログインしたときに飛ばされる場所

    public static Coordinate3D respawnPoint; //死んだときに飛ばされる場所

    public static Coordinate3D lobbyPos1; //ロビー座標1
    public static Coordinate3D lobbyPos2; //ロビー座標2

    public static int untilCombatable; //攻撃可能になるまでの時間
    public static int addDeathAreaInterval; //禁止区域が追加される間隔


    public static void setWorldFromString(String worldName){
        world = BattleRoyale.getINSTANCE().getServer().getWorld(worldName);
    }
}
