package com._28ftb.plugin.database.data;

public enum PlayerStatus {
    ALIVE,
    DEATH,
    OPERATOR,
    FUTABA,
    SPECTATOR,
}
