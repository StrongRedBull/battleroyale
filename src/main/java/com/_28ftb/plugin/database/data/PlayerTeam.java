package com._28ftb.plugin.database.data;


/**
 * プレイヤーのチームらしい。チーム戦用？
 */
public enum PlayerTeam {
    RED,
    BLUE,
    YELLOW,
    GREEN
}
