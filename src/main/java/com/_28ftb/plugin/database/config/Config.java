package com._28ftb.plugin.database.config;


import com._28ftb.plugin.BattleRoyale;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.nio.charset.StandardCharsets;
import java.util.logging.Level;

public class Config {
    private FileConfiguration config = null;
    private final File configFile;
    private final String file;

    public Config(String fileName) {
        this.file = fileName;
        configFile = new File(BattleRoyale.getINSTANCE().getDataFolder(), file);
    }

    //デフォルト設定を保存
    public void saveDefaultConfig() {
        if (!configFile.exists()) {
            BattleRoyale.getINSTANCE().saveResource(file, false);
        }
    }

    //設定を返す
    public FileConfiguration getConfig() {
        if (config == null) {
            reloadConfig();
        }
        return config;
    }

    //設定を保存
    public void saveConfig() {
        if (config == null) {
            return;
        }

        try {
            getConfig().save(configFile);
        } catch (IOException ex) {
            BattleRoyale.getINSTANCE().getLogger().log(Level.SEVERE, "Could not save config to " + configFile, ex);
        }
    }

    //設定をリロード
    public void reloadConfig() {
        config = YamlConfiguration.loadConfiguration(configFile);

        final InputStream defConfigStream = BattleRoyale.getINSTANCE().getResource(file);
        if (defConfigStream == null) {
            return;
        }

        config.setDefaults(YamlConfiguration.loadConfiguration(new InputStreamReader(defConfigStream, StandardCharsets.UTF_8)));
    }
}
