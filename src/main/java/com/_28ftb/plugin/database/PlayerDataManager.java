package com._28ftb.plugin.database;

import com._28ftb.plugin.BattleRoyale;
import com._28ftb.plugin.database.data.PlayerData;
import com._28ftb.plugin.database.data.PlayerStatus;
import com._28ftb.plugin.utils.MetaUtil;
import org.bukkit.entity.Player;

public class PlayerDataManager {
    public static MetaUtil<PlayerData> metaPlayerData = new MetaUtil<>(BattleRoyale.getINSTANCE());


    public static void setPlayerData(Player player,PlayerData data){
        metaPlayerData.setMeta(player,"playerData",data);
    }

    public static PlayerData getPlayerData(Player player){
        return metaPlayerData.getMeta(player,"playerData");
    }

}
