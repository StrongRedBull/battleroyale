package com._28ftb.plugin.game;

import com._28ftb.plugin.BattleRoyale;
import lombok.Getter;
import lombok.Setter;
import org.bukkit.scheduler.BukkitRunnable;

public class GameClock {
    @Getter
    private int time = 0;
    @Setter
    private boolean cancelFlag = false;

    public GameClock(){
        this.startClock();
    }

    private void startClock(){
        new BukkitRunnable(){
            @Override
            public void run() {
                time++;
                if(cancelFlag){
                    this.cancel();
                }
            }
        }.runTaskTimer(BattleRoyale.getINSTANCE(),0,1);
    }
}
