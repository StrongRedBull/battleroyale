package com._28ftb.plugin.commands;

import com._28ftb.plugin.BattleRoyale;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;

import java.util.List;

public class TabCompleter implements org.bukkit.command.TabCompleter {
    public TabCompleter(){
        BattleRoyale.getINSTANCE().getCommand("battleroyale").setTabCompleter(this);
    }

    @Override
    public List<String> onTabComplete(CommandSender sender, Command command, String alias, String[] args) {
        return null;
    }
}
