package com._28ftb.plugin.utils;

import com._28ftb.plugin.BattleRoyale;
import org.apache.commons.lang.math.DoubleRange;
import org.apache.commons.lang.math.IntRange;
import org.bukkit.Location;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class PlayerUtil {
    /**
     * プレイヤーのインベントリにアイテムを追加します。 //TODO: 未検証
     *
     * @param player     プレイヤー
     * @param itemStacks 追加するアイテム
     */
    public static void addItems(@NotNull Player player, ItemStack... itemStacks) {
        player.getInventory().addItem(itemStacks);
    }


}
