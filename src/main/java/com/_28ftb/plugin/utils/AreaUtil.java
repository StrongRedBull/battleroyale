package com._28ftb.plugin.utils;

import com._28ftb.plugin.BattleRoyale;
import org.apache.commons.lang.math.DoubleRange;
import org.bukkit.Location;
import org.bukkit.entity.Player;

import java.util.ArrayList;
import java.util.List;

public class AreaUtil {
    public static List<Player> getPlayersInArea(Location loc1, Location loc2){
        List<Player> playerList = new ArrayList<>();
        DoubleRange rangeX = new DoubleRange(loc1.getX(),loc2.getX());
        DoubleRange rangeZ = new DoubleRange(loc1.getZ(),loc2.getZ());

        for(Player player: BattleRoyale.getINSTANCE().getServer().getOnlinePlayers()){
            if(rangeX.containsDouble(player.getLocation().getX()) && rangeZ.containsDouble(player.getLocation().getZ())){
                playerList.add(player);
            }
        }
        return playerList;
    }
}
