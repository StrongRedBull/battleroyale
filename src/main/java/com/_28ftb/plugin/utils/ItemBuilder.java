package com._28ftb.plugin.utils;

import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.jetbrains.annotations.NotNull;

import java.util.List;

public class ItemBuilder {
    private ItemStack itemStack;

    public ItemBuilder(@NotNull Material material) {
        this.itemStack = new ItemStack(material);
    }

    @NotNull
    public ItemBuilder setAmount(int amount) {
        this.itemStack.setAmount(amount);
        return this;
    }

    @NotNull
    public ItemBuilder setDurability(int durability) {
        this.itemStack.setDurability((short) durability);
        return this;
    }

    @NotNull
    public ItemBuilder setDurability(short durability) {
        this.itemStack.setDurability(durability);
        return this;
    }

    @NotNull
    public ItemBuilder setDisplayName(@NotNull String name) {
        ItemMeta meta = this.itemStack.getItemMeta();
        meta.setDisplayName(ChatColor.RESET + name);
        this.itemStack.setItemMeta(meta);
        return this;
    }

    @NotNull
    public ItemBuilder setRawDisplayName(@NotNull String name) {
        ItemMeta meta = this.itemStack.getItemMeta();
        meta.setDisplayName(name);
        this.itemStack.setItemMeta(meta);
        return this;
    }

    @NotNull
    public ItemBuilder setLore(@NotNull List<String> content) {
        ItemMeta meta = this.itemStack.getItemMeta();
        meta.setLore(content);
        this.itemStack.setItemMeta(meta);
        return this;
    }

    @NotNull
    public ItemBuilder setUnbreakable() {
        ItemMeta meta = this.itemStack.getItemMeta();
        meta.setUnbreakable(true);
        this.itemStack.setItemMeta(meta);

        return this;
    }

    public ItemStack build() {
        return this.itemStack;
    }
}
