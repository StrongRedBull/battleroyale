package com._28ftb.plugin.utils;

import org.bukkit.inventory.ItemFlag;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.jetbrains.annotations.NotNull;

import java.util.List;

public class ItemUtil {
    private ItemUtil() {
    }

    @NotNull
    public static ItemStack setDisplayName(@NotNull ItemStack stack, @NotNull String name) {
        ItemMeta meta = stack.getItemMeta();
        meta.setDisplayName(name);
        stack.setItemMeta(meta);
        return stack;
    }

    @NotNull
    public static ItemStack setLore(@NotNull ItemStack stack, @NotNull List<String> content) {
        ItemMeta meta = stack.getItemMeta();
        meta.setLore(content);
        stack.setItemMeta(meta);
        return stack;
    }

    @NotNull
    public static ItemStack toUnbreakable(@NotNull ItemStack stack) {
        ItemMeta meta = stack.getItemMeta();
        meta.setUnbreakable(true);
        stack.setItemMeta(meta);
        return stack;
    }

    @NotNull
    public static ItemStack hideAllFlags(@NotNull ItemStack stack) {
        ItemMeta meta = stack.getItemMeta();
        meta.addItemFlags(ItemFlag.HIDE_ATTRIBUTES, ItemFlag.HIDE_DESTROYS, ItemFlag.HIDE_PLACED_ON, ItemFlag.HIDE_UNBREAKABLE);
        stack.setItemMeta(meta);
        return stack;
    }
}
