package com._28ftb.plugin.utils;

import com._28ftb.plugin.BattleRoyale;
import org.bukkit.entity.Entity;
import org.bukkit.metadata.FixedMetadataValue;
import org.bukkit.plugin.Plugin;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

/**
 * メタデータを操作するユーテリティです。
 * メタデータとして、エンティティにデータを保存することが出来ます。
 * 全てのメタデータは、サーバー再起動・リロード時に削除されます。
 *
 * @param <T> メタデータとして保存する型
 */
public class MetaUtil<T> {
    private Plugin plugin;

    /**
     * MetaUtilを初期化します。
     *
     * @param plugin プラグインのインスタンス
     */
    public MetaUtil(@NotNull Plugin plugin) {
        this.plugin = plugin;
    }

    /**
     * MetaUtilをGentai-APIのインスタンスで初期化します。
     * 注意: 名前衝突を起こす可能性があります。
     */
    public MetaUtil() {
        this.plugin = BattleRoyale.getINSTANCE();
    }

    /**
     * メタデータをエンティティにセットします。
     *
     * @param entity エンティティ
     * @param key    キー
     * @param value  値
     */
    public void setMeta(@NotNull Entity entity, @NotNull String key, @NotNull T value) {
        entity.setMetadata(key, new FixedMetadataValue(this.plugin, value));
    }

    /**
     * エンティティがメタデータを持っているかをチェックします。
     *
     * @param entity エンティティ
     * @param key    キー
     * @return 持っていたらtrue, 持っていないならfalse
     */
    public boolean hasMeta(@NotNull Entity entity, @NotNull String key) {
        return entity.hasMetadata(key);
    }

    /**
     * エンティティからメタデータを削除します。
     *
     * @param entity エンティティ
     * @param key    キー
     */
    public void removeMeta(@NotNull Entity entity, @NotNull String key) {
        entity.removeMetadata(key, this.plugin);
    }

    /**
     * エンティティからメタデータを取得します。
     *
     * @param entity エンティティ
     * @param key    キー
     * @return 取得したメタデータ。存在しない場合はnull
     */
    @Nullable
    @SuppressWarnings("unchecked")
    public T getMeta(@NotNull Entity entity, @NotNull String key) {
        if (!entity.hasMetadata(key))
            return null;

        return (T) entity.getMetadata(key).get(0).value();
    }
}