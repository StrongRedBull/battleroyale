package com._28ftb.plugin.clubs.data;

public enum ClubType {
    ARCHERY,
    BASEBALL,
    DRAMA,
    BASKETBALL,
    VOLLEYBALL,
    SOCCER,
    TRACK_AND_FIELD,
    KARATE,
    SWIMMING,
    KENDOU,
    SCIENCE,
    TEA_CEREMONY

}
